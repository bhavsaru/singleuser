#!/bin/sh
../tomcat/bin/shutdown.sh
mvn clean package
rm -rf ../tomcat/webapps/USER-1.0-SNAPSHOT*
rm -f ../tomcat/resources/*
cp target/USER-1.0-SNAPSHOT.war ../tomcat/webapps/
cp -r src/main/resources/* ../tomcat/resources/
../tomcat/bin/startup.sh
