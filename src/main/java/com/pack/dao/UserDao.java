package com.pack.dao;

import com.pack.model.User;

import java.util.List;
import java.util.Map;

/**
 * Created by rbhavsar on 1/12/16.
 */
public interface UserDao {

    List<User> getAllUsers();

    List<Map<String, Object>> getAll () ;

    User getuser(int id);

    void deleteUser(int id);

    void addUser(User user);

    void updateUser(User user);


}
