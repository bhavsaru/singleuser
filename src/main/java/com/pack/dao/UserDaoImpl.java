package com.pack.dao;

import com.pack.model.User;
import org.omg.PortableInterceptor.USER_EXCEPTION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;

/**
 * Created by rbhavsar on 1/12/16.
 */
//@Repository("userDao")
public class UserDaoImpl implements UserDao {


    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<User> getAllUsers() {

        int rowCount = jdbcTemplate.queryForObject("select count(*) from user", Integer.class);

         System.out.println(rowCount);

          List<User> userlist = new ArrayList<User>(1);
         // userlist.add(new User());
          userlist = jdbcTemplate.query("select * from user",
                  new RowMapper<User>() {
                      public User mapRow(ResultSet resultSet, int i) throws SQLException {
                          User user = new User();
                          user.setId(resultSet.getInt("ID"));
                          user.setFname(resultSet.getString("FNAME"));
                          user.setLname(resultSet.getString("LNAME"));
                          user.setEmail(resultSet.getString("EMAIL"));
                          return user;
                      }
                  });

        return userlist;
    }

    public List<Map<String, Object>> getAll() {

        String sql = "SELECT * FROM user";

        List<User> user = new ArrayList<User>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

        return rows;

    }

    public User getuser(int id) {

        User user = new User();

        String sql = "SELECT * FROM USER WHERE ID = ?";

         user = jdbcTemplate.queryForObject(sql, new Object[]{id}, new RowMapper<User>() {
            public User mapRow(ResultSet resultSet, int i) throws SQLException {

                User user = new User();
                user.setId((int)resultSet.getInt("ID"));
                user.setFname(resultSet.getString("FNAME"));
                user.setLname(resultSet.getString("LNAME"));
                user.setEmail(resultSet.getString("EMAIL"));

                return user;
            }
        });


        return user;
    }

 //   delete user
    public void deleteUser(int id) {

        String sql = "DELETE FROM USER WHERE ID=?";
        jdbcTemplate.update(sql,id);


    }
//    add user
    public void addUser(User user) {

        String sql = "INSERT INTO USER (fname, lname, email, phone) VALUES (?, ?, ?, ?)";
        jdbcTemplate.update(sql,user.getFname(),user.getLname(),user.getEmail());

    }

    public void updateUser(User user) {

        String sql = "UPDATE USER SET FNAME=?, LNAME=?, EMAIL=?, PHONE=? WHERE ID=?";
        jdbcTemplate.update(sql, new Object[] {user.getFname(),user.getLname(),user.getEmail(),user.getId()});

    }

}
