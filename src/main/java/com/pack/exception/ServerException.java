package com.pack.exception;

/**
 * Created by rbhavsar on 1/28/16.
 */
public class ServerException extends BaseException {
    public ServerException(String s, Exception e) {
        super(s, e);
    }
}
