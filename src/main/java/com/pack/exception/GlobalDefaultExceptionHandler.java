package com.pack.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by rbhavsar on 1/28/16.
 *
 */
// Global Exception Handler , It will handle exceptions from the whole application
@ControllerAdvice
class GlobalDefaultExceptionHandler {
    public static final String DEFAULT_ERROR_VIEW = "error";

    @ExceptionHandler(value = ClientException.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest req, ClientException e) throws Exception {

        ModelAndView modelandview = new ModelAndView();

        modelandview.addObject("message" , e.getMessage());
        modelandview.setViewName("clientexception");

        return modelandview;


       }

    @ExceptionHandler(value = UserNotFoundException.class)
    public ModelAndView defaultErrorHandler2(HttpServletRequest req, HttpServletResponse res, UserNotFoundException e) throws Exception {

        res.setStatus(HttpServletResponse.SC_NOT_FOUND);
        ModelAndView modelandview = new ModelAndView();

        modelandview.addObject("message" , e.getMessage());
        modelandview.setViewName("clientexception");

        return modelandview;

    }

    @ExceptionHandler(value = Exception.class)
    public ModelAndView defaultErrorHandler3(HttpServletRequest req, ClientException e,HttpServletResponse res) throws Exception {

        ModelAndView modelandview = new ModelAndView();
        res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

        modelandview.addObject("message" , e.getMessage());
        modelandview.setViewName("clientexception");

        return modelandview;

    }

}