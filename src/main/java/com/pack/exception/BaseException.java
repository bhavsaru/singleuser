package com.pack.exception;

/**
 * Created by rbhavsar on 1/28/16.
 */
public class BaseException extends RuntimeException {
    public BaseException(String s, Exception e) {
        super(s, e);
    }
}
