package com.pack.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by rbhavsar on 1/28/16.
 */
@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="No such User")  // 404
public class UserNotFoundException extends RuntimeException {

   public UserNotFoundException(String message, Exception e){

       super(message, e);

   }

    public UserNotFoundException(String message){

        super(message);

    }


    @Override
    public String toString() {
        return "UserNotFoundException{" +
                ", message='" + getMessage() + '\'' +
                '}';
    }
}
