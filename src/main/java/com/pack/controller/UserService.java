package com.pack.controller;

import com.pack.logic.UserLogic;
import com.pack.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * Created by rbhavsar on 1/24/16.
 */
//@CrossOrigin
@RequestMapping("/userservice")
@RestController
public class UserService {

  @Autowired
  UserLogic userLogic;


  @RequestMapping(value = "/users" , method = RequestMethod.GET ,produces = "application/json")
  public ResponseEntity<List<User>> getUserList()
  {
      List<User> userList = userLogic.getlist();
      return new ResponseEntity <List<User>>(userList, HttpStatus.OK);
  }


   @RequestMapping(value = "/users/{userid}" , method = RequestMethod.GET , produces = "application/json" )
   public ResponseEntity<User> getUser(@PathVariable int userid)
   {
          User user = null;
          user = userLogic.getUser(userid);

       return new ResponseEntity <User>(user,HttpStatus.OK);
   }

   @RequestMapping(value = "users/{userid}" , method = RequestMethod.DELETE )
   public ResponseEntity deleteUser(@PathVariable int userid)
   {
        userLogic.deleteUser(userid);
        return new ResponseEntity(HttpStatus.OK);
   }


   @RequestMapping(value = "/users" , method = RequestMethod.POST)
   public ResponseEntity saveUser(@RequestBody User user)
   {

       userLogic.addUser(user);
       return new ResponseEntity(HttpStatus.OK);
   }

   @RequestMapping(value = "/users/{userid}" , method = RequestMethod.PUT)
   public ResponseEntity updateUser(@PathVariable int userid , @RequestBody User user)
    {

        userLogic.updateUser(user);
        return new ResponseEntity(HttpStatus.OK);
    }


}
