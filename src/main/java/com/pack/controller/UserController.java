package com.pack.controller;

import com.pack.exception.UserNotFoundException;
import com.pack.model.Address;
import com.pack.model.Phone;
import com.pack.model.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rbhavsar on 1/12/16.
 */
@Controller
public class UserController {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${user.rest.host}")
    private String host;

    @Value("${user.rest.uri}")
    private String uri;

    @Value("${user.rest.uriid}")
    private String uriid;



    Logger logger = LogManager.getLogger(getClass());

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ModelAndView getUsers()
    {

      logger.info("getUserList started.");
        ModelAndView model = new ModelAndView();
         final String url = host+uri ;
        List<User> userList = restTemplate.getForObject(url, List.class);


        model.addObject("users",  userList);
        model.setViewName("userlist");

        return model;

    }

    @RequestMapping(value = "/user/getuser", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public User getUser(@RequestParam int id)
    {
        System.out.println("Get user method called");

        User user = new User();

        final String url = host+uriid ;
       try {
           user = restTemplate.getForObject(url, User.class, id);
       }
       catch (Exception e)
       {

        throw new UserNotFoundException("User Not Found",e);
        //  e.printStackTrace();
       }
        return user;
         }


    @RequestMapping(value = "/user/deleteuser")
    public ModelAndView deleteuser(@RequestParam int id)
    {

        final String url = host+uriid ;
        restTemplate.delete(url,id);
        return new ModelAndView("redirect:/user.do");


    }

   @RequestMapping(value = "/user/adduser" , method = RequestMethod.POST )
    public ModelAndView addUser(@ModelAttribute User user , HttpServletRequest request)
    {
        System.out.println("Add user method called");

        String mobile = request.getParameter("mobile");
        String othermob = request.getParameter("other");

        List<Phone> phones = new ArrayList<Phone>();

        Address address = new Address();
        address.setAdrline1(request.getParameter("addadrline1"));

        if (mobile!= null && !mobile.equals(""))
       {
           int mob = Integer.parseInt(mobile);
           phones.add(new Phone(mob , "mob"));
       }
       if (othermob!= null && !othermob.equals(""))
       {
           int other  = Integer.parseInt(othermob);
           phones.add(new Phone(other , "oth"));
       }

        if (phones.size() > 0 ) {
            user.setPhones(phones);
        }

        user.setAddress(address);

         final String url = host+uri ;
         restTemplate.postForLocation(url,user,User.class);




     return new ModelAndView("redirect:/user.do");
    }

    @RequestMapping(value = "/user/updateuser" , method = RequestMethod.POST)
    public ModelAndView updateUser(User user , HttpServletRequest request)
    {

        int id = user.getId();
        int adrid;

        System.out.println("update user method called");
        String mobile = request.getParameter("editphonemob");
        String othermob = request.getParameter("editphoneother");

       int mobileid = Integer.parseInt(request.getParameter("editmobid"));
       int otherphoneid = Integer.parseInt(request.getParameter("editphoneothid"));


        Address address = new Address();
        address.setAdrline1(request.getParameter("editadrline1"));

        List<Phone> phones = new ArrayList<Phone>();


        if (mobile!= null && !mobile.equals("")) {
            int mob = Integer.parseInt(mobile);
            phones.add(new Phone(mobileid,mob , "mob"));
        }
        if (othermob!=null && !othermob.equals("")) {
            int other  = Integer.parseInt(othermob);
            phones.add(new Phone(otherphoneid,other , "oth"));
        }

        if (phones.size() > 0 ) {
            user.setPhones(phones);
        }

        adrid = Integer.parseInt(request.getParameter("editadrid"));
        address.setAdrid(adrid);
        address.setAdrline1(request.getParameter("editadrline1"));

        user.setPhones(phones);
        user.setAddress(address);
        final String url = host+uriid ;
        restTemplate.put(url, user, id);
        return new ModelAndView("redirect:/user.do");

    }

//    It will handle exceptions from this controller
    @ExceptionHandler(UserNotFoundException.class)
    public ModelAndView conflict(UserNotFoundException exception , HttpServletResponse response) {

        exception.printStackTrace();
        ModelAndView mav = new ModelAndView();
        mav.setViewName("clientexception");
        mav.addObject("message" , exception);
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        return mav ;

    }

}
