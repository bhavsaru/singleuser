package com.pack.hibernatedao;

import com.pack.model.User;

import java.util.List;
import java.util.Map;

/**
 * Created by rbhavsar on 1/14/16.
 */
public interface UserDao {

    List<User> getAllUsers();

    User getuser(int id);

    void deleteUser(int id);

    void addUser(User user);

    void updateUser(User user);

}
