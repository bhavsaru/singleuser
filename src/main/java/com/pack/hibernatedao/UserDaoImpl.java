package com.pack.hibernatedao;

import com.pack.model.User;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rbhavsar on 1/14/16.
 */

@Repository("userDao")
public class UserDaoImpl implements UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    public List<User> getAllUsers() {

        List<User> users = new ArrayList<User> ();
        try{
        users= entityManager.createQuery("from User u").getResultList();

        }
        catch (HibernateException e)
        {
            e.printStackTrace();
        }
        return users;
    }

    public User getuser(int id) {

        User user = new User();

        user = entityManager.find(User.class , id) ;

        return user;
    }

    public void deleteUser(int id) {
        User user = new User();

        try {
            user  =(User)entityManager.find(User.class,id);
            entityManager.remove(user);

        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }

    public void addUser(User user) {
        try {

            entityManager.persist(user);


        } catch (HibernateException e) {
            e.printStackTrace();
            throw e;
        }

    }

    public void updateUser(User user) {

        try {

            entityManager.merge(user);

        } catch (HibernateException e) {
            e.printStackTrace();
            throw e;
        }

    }
}
