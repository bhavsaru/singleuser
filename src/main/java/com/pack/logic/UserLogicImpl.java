package com.pack.logic;

import com.pack.exception.ClientException;
import com.pack.exception.UserNotFoundException;
import com.pack.hibernatedao.UserDao;
import com.pack.model.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rbhavsar on 1/12/16.
 */

@Service("userLogic")
public class UserLogicImpl implements UserLogic{

    @Autowired
    UserDao userDao;

    Logger logger = LogManager.getLogger(getClass());

    @Transactional
    public List<User> getlist()
    {
        User u1 = new User();
        List<User> userlist = new ArrayList<User>();

        userlist = userDao.getAllUsers();

        int size ;  //For lazyloading we need to call size method

        for (int i = 0; i < userlist.size(); i++) {

           size = userlist.get(i).getPhones().size();  //For lazyloading we need to call size method
        }

        return userlist;
    }

    @Transactional(readOnly = true)
    public User getUser(int id)  {
        User user = new User();


        try {
            user = userDao.getuser(id);
        }
        catch (Exception e)
        {
            throw new ClientException("Some Error in user retrival",e);
        }


        if(user == null) {
            throw new UserNotFoundException("User not found");
        }
            int size = user.getPhones().size();
        return user;
    }
//
//    public void getMultiThreaded() {
//        List<User> allUsers = new ArrayList<User>();
//        MyThread1 t1 = new MyThread1(allUsers);
//        MyThread2 t2 = new MyThread2();
//        Thread th1 = new Thread(t1);
//        Thread th2 = new Thread(t2);
//
//        th1.start();
//        th2.start();
//        allUsers.addAll(t1.users);
//        allUsers.addAll(t2.users);
//    }
//
//    class MyThread1 implements Runnable {
//        List<User> users;
//        MyThread1(List<User> users) {
//            this.users = users;
//        }
//        public void run() {
//
//                users.addAll(userDao.getAllUsers());
//        }
//    }
//
//    class MyThread2 implements Runnable {
//        List<User> users;
//        public void run() {
//
//            users= userDao.getAllUsers();
//        }
//    }

    @Transactional
    public void deleteUser(int id) {

        userDao.deleteUser(id);
    }

    @Transactional
    public void addUser(User user) {

        userDao.addUser(user);
    }

    @Transactional
    public void updateUser(User user)  {
        try {

            userDao.updateUser(user);
        } catch (Exception e) {
            logger.error("Error happened during update " + e.getMessage(), e);
            throw new ClientException("Error while updating User ", e);
        }

    }
}
