package com.pack.logic;
import com.pack.model.User;
import java.util.List;



/**
 * Created by rbhavsar on 1/12/16.
 */
public interface UserLogic {
    public List<User> getlist();

    public User getUser(int id);      //It will get a user object

    public void deleteUser(int id);

    public void addUser(User user);

    public void updateUser(User user) ;

}


