package com.pack.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by rbhavsar on 1/20/16.
 */

@Entity
@Table(name = "ADDRESS")
public class Address implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int adrid;


    @Column(name = "ADRLINE1")
    private String adrline1;

//    @Column(name = "ADRLINE2")
//    private String adrline2;
//
//    @Column(name = "CITY")
//    private String city;
//
//    @Column(name = "STATE")
//    private String state;
//
//    @Column(name = "COUNTRY")
//    private String country;
//
//    @Column(name = "ZIP")
//    private int zip;

    public int getAdrid() {
        return adrid;
    }

    public void setAdrid(int adrid) {
        this.adrid = adrid;
    }

    public String getAdrline1() {
        return adrline1;
    }

    public void setAdrline1(String adrline1) {
        this.adrline1 = adrline1;
    }

//    public String getAdrline2() {
//        return adrline2;
//    }
//
//    public void setAdrline2(String adrline2) {
//        this.adrline2 = adrline2;
//    }
//
//    public String getCity() {
//        return city;
//    }
//
//    public void setCity(String city) {
//        this.city = city;
//    }
//
//    public String getState() {
//        return state;
//    }
//
//    public void setState(String state) {
//        this.state = state;
//    }
//
//    public String getCountry() {
//        return country;
//    }
//
//    public void setCountry(String country) {
//        this.country = country;
//    }
//
//    public int getZip() {
//        return zip;
//    }
//
//    public void setZip(int zip) {
//        this.zip = zip;
//    }
//
//

}
