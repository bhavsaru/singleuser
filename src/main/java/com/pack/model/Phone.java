package com.pack.model;

import javax.persistence.*;

/**
 * Created by rbhavsar on 1/18/16.
 */

@Entity
@Table(name = "PHONES")
public class Phone {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PHONEID" , nullable = false)
    private int phoneid;

    @Column(name = "PHONENO")
    private int phoneNo;

    @Column(name = "PHONETYPE")
    private String phoneType;

    public Phone(){};

    public Phone(int phoneid ,int phoneno , String phonetype){
        this.phoneid = phoneid;
        this.phoneNo = phoneno;
        this.phoneType = phonetype;
    };

    public Phone(int phoneno , String phonetype){
        this.phoneNo = phoneno;
        this.phoneType = phonetype;
    };


    public int getPhoneid() {
        return phoneid;
    }

    public void setPhoneid(int phoneid) {
        this.phoneid = phoneid;
    }

    public int getPhoneNo() {
        return phoneNo;

    }

    public void setPhoneNo(int phoneNo) {
        this.phoneNo = phoneNo;
    }


}
