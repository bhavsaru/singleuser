package com.pack.model;

import javax.persistence.*;
import java.util.List;
import org.hibernate.annotations.Cascade;
/**
 * Created by rbhavsar on 1/12/16.
 */

@Entity
@Table(name = "USER")
public class User {

     @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
     @Column(name = "ID" , nullable = false)
     private int id;

     @Column(name ="FNAME")
     private String fname;

     @Column(name ="LNAME")
     private String lname;

     @Column(name ="EMAIL")
     private String email;


    @OneToMany(fetch = FetchType.LAZY ,cascade = {CascadeType.ALL ,CascadeType.MERGE})
    @JoinColumn(name = "ID")
    private List<Phone> phones;


    @OneToOne(fetch = FetchType.EAGER , cascade={ CascadeType.ALL, CascadeType.REMOVE})
    @JoinColumn(name = "ADRID")
    private Address address;


    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }


    @Override
    public boolean equals(Object obj) {

       if (obj == null)
       {
           return false;
       }

       if( !(obj instanceof User) )
       {
           return false;
       }
       User user = (User)obj;
       if(this.id == user.id)
       {
           return true;
       }else
       {
           return false;
       }
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    @Override
    public String toString() {

        String user = this.id + " ," + this.fname + "," + this.lname ;
        return user;

    }
}
