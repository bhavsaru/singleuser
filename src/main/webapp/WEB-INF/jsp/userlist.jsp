<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<html>
<head>
<style>

.btn {
  background: #3498db;
  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
  background-image: -o-linear-gradient(top, #3498db, #2980b9);
  background-image: linear-gradient(to bottom, #3498db, #2980b9);
  -webkit-border-radius: 28;
  -moz-border-radius: 28;
  border-radius: 28px;
  -webkit-box-shadow: 8px 3px 7px #666666;
  -moz-box-shadow: 8px 3px 7px #666666;
  box-shadow: 8px 3px 7px #666666;
  font-family: Arial;
  color: #ffffff;
  font-size: 20px;
  padding: 10px 10px 10px 10px;
  text-decoration: none;
}

.btn:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
}

.divmain{
    margin-right: 10%;
    margin-left: 10%;
}

label{
color: black ;
}



</style>
 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script>


     function delfun(uid){

         if (confirm("Are you sure ?")) {

           $.ajax({
   		   url : "user/deleteuser.do",
   		   data : {
   		   "id" : uid
   				  },

   			}).done(function(data){

             location.reload(true);

   		   });

          }

     }

    function change(id){

         var adrid = $('#adr'+id).val();


           $.ajax({
   	       type : "GET",
   		   url : "user/getuser.do",
   		   data : {
   		   "id" : id
   				  },

   			})
   			.fail(function(){
   			    alert('failed');
   			})
   			.done(function(data){

                    $('#editid').val() ;
                    $('#editfname').val() ;
                    $('#editlname').val() ;
                    $('#editemail').val() ;
                    $('#editphone').val() ;
                    $('#editphonemob').val() ;
                    $('#editphoneother').val() ;
                    $('#editadrline1').val();
                    $('#editadrid').val();
                     $('#editid').val(data.id) ;
                     $('#editfname').val(data.fname) ;
                     $('#editlname').val(data.lname) ;
                     $('#editemail').val(data.email) ;
                     $('#editphone').val(data.phone) ;
                     if (data.phones != null)
                     {
                      if (data.phones[0].phoneNo != null)
                      {
                       $('#editphonemob').val(data.phones[0].phoneNo) ;
                       $('#editmobid').val(data.phones[0].phoneid);

                      }
                      if (data.phones[1].phoneNo != null)
                      {
                       $('#editphoneother').val(data.phones[1].phoneNo) ;
                       $('#editphoneothid').val(data.phones[1].phoneid);
                       }

                      }
                     if ( data.address != null )
                     {

                     $('#editadrline1').val(data.address.adrline1);
                     }
                     $('#editadrid').val(adrid);
                     $('#editmodal').modal('show');
   		   });
     }

  </script>



</head>
<body class="body">
<div class="divmain">
  <div class="add" align="right">
  <button class="btn" data-toggle="modal" data-target="#addmodal" >ADD</button>
   <!--  <input type="button" value="ADD" class="btn"> -->
  </div>
            <table class="table table-striped" border="1px">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>AddressLine1</th>
                        <th>Email/login</th>
                        <th>Mobile</th>
                        <th>Other</th>
                        <th>Select</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="user" items="${users}">
                        <tr>
                            <td>${user.id}</td>
                            <td>${user.fname}</td>
                            <td>${user.lname}</td>
                            <td>${user.address.adrline1}<input type="hidden" id="adr${user.id}" name="adr${user.id}" value="${user.address.adrid}"/></td>
                            <td>${user.email}</td>

                            <c:forEach var="phone" items="${user.phones}">

                            <td>${phone.phoneNo}<input type="hidden" value="${phone.phoneid}"></td>

                            </c:forEach>
                            <c:if test="${fn:length(user.phones) < 2 }">
                            <td> </td>
                              <c:if test="${fn:length(user.phones) < 1 }">
                              <td> </td>

                              </c:if>


                            </c:if>


                            <td>
                            <button onclick="change('${user.id}')"
                            class="btn"
                            name="update_${user.id}" id="update_${user.id}">Update</button>

                            <button onclick="delfun('${user.id}')"
                            class="btn"
                            name="delete_${user.id}"  id="delete_${user.id}">Delete</button>

                            </td>

                        </tr>
                    </c:forEach>
                </tbody>
            </table>
  </div>
   <div id="editmodal" class="modal fade" role="dialog">
     <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          <h4 class="modal-title" class="myModalLabel">Update User</h4>
          </div>
           <div class="modal-body">
            <form action="user/updateuser.do" method="POST">
            <table>
              <tr><td><label>Id</label></td><td><input id="editid" name="id" readonly/></td></tr>
              <tr><td><label>Fname</label></td><td><input id="editfname" name="fname"/></td></tr>
              <tr><td><label>Lname</label></td><td><input id="editlname" name="lname"/></td></tr>
              <tr><td><label>Email</label></td><td><input id="editemail" name="email"/></td></tr>
              <tr><td><label>Mobile</label></td>
                  <td><input id="editphonemob" name="editphonemob"/></td>
                      <input type="hidden" id="editmobid" name="editmobid">

                  </tr>
              <tr><td><label>Other</label></td><td><input id="editphoneother" name="editphoneother"/></td>
                      <input type="hidden" id="editphoneothid" name="editphoneothid">
                   </tr>
              <tr><td><label>AddressLine1</label></td>
                  <td><input id="editadrline1" name="editadrline1"/></td>
                  <input type="hidden" name="editadrid" id="editadrid"/></tr>

            </table>
            <button type="submit" name="updatebtn" id="updatebtn">Submit</button>
            </form>
            </div>
        </div>
     </div>
    </div>


<!-- Add Dialog -->

<div id="addmodal" class="modal fade" role="dialog">
     <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          <h4 class="modal-title" class="addmodallabel">Register User</h4>
          </div>
           <div class="modal-body">
           <form action="user/adduser.do" method="POST" enctype='application/json'>
            <table>
              <tr><td><input id="editid" readonly type="hidden"/></td></tr>
              <tr><td><label>Fname</label></td><td><input id="editfname" name="fname"/></td></tr>
              <tr><td><label>Lname</label></td><td><input id="editlname" name="lname"/></td></tr>
              <tr><td><label>Email</label></td><td><input id="editemail" name="email"/></td></tr>
              <tr><td><label>Mobile</label></td><td><input id="mobile" name="mobile"/></td></tr>
              <tr><td><label>Other</label></td><td><input id="other" name="other"/></td></tr>

              <tr><td><label>Address line1</label></td><td><input id="addadrline1" name="addadrline1"/></td></tr>
       <!--          <tr><td><label>Address line2</label></td><td><input id="addadrline2" name="addadrline2"/></td></tr>
              <tr><td><label>City</label></td><td><input id="addcity" name="addcity"/></td></tr>
              <tr><td><label>State</label></td><td><input id="addstate" name="addstate"/></td></tr>
              <tr><td><label>Country</label></td><td><input id="addcountry" name="addcountry"/></td></tr>
              <tr><td><label>AreaCode</label></td><td><input id="addcode" name="addcode"/></td></tr>
-->
            </table>
             <button type="submit" name="registerbtn" id="registerbtn">Submit</button>
            </form>
            </div>
        </div>
     </div>
    </div>


<!--Confirm delete dialog----------------

<div class="delmodal" class="modal fade" role="dialog">
     <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          <h4 class="modal-title" class="addmodallabel">Register User</h4>
          </div>
           <div class="modal-body">
             <label> Are you sure you want to delete this record ? </label>
            <table>
            <tr><td> <button type="submit" name="confirmbtn" id="confirmbtn" onclick="delfun">Ok</button>
                </td>
                <td>
                 <button type="cancel" name="cancelbtn" id="cancelbtn">Cancel</button>
                </td>
            </tr>
             </table>
            </div>
        </div>
     </div>
</div>
-->



 </body>
</html>
