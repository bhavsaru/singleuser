package com.pack.controller;

import com.pack.logic.UserLogic;
import com.pack.model.Address;
import com.pack.model.Phone;
import org.mockito.internal.stubbing.answers.DoesNothing;
import org.springframework.web.client.RestTemplate;
import org.testng.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import java.util.ArrayList;
import java.util.List;
import com.pack.model.User;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import org.springframework.mock.web.MockHttpServletRequest;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;


/**
 * Created by rbhavsar on 1/22/16.
 */

public class UserControllerTest {


    @Mock
    UserLogic userLogic;

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    UserController usercontroller;

    @Spy
    List<User> users = new ArrayList<User>();

    @Mock
    BindingResult result;

    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);  // common things for all methods
        users = getUserList();
//        modelAndView = getModalandview();
    }


    @Test
   public void testListUsers()
    {

//         when(userLogic.getlist()).thenReturn(users);
        final String url = "http://localhost:8080/USER-1.0-SNAPSHOT/userservice/users.do";
        when(restTemplate.getForObject(url, List.class)).thenReturn(users);

        ModelAndView model = usercontroller.getUsers();

        Assert.assertTrue((model.getViewName().equals("userlist")));
        Assert.assertEquals(model.getModel().get("users"),users);
        verify(restTemplate,atLeastOnce()).getForObject(url,List.class);


    }

    @Test
   public void testGetUser()
    {

        User user = new User();
        user = users.get(0);
//        when(userLogic.getUser(anyInt())).thenReturn(user);

         final String url = "http://localhost:8080/USER-1.0-SNAPSHOT/userservice/users/{userid}.do";
         when(restTemplate.getForObject(url, User.class ,1)).thenReturn(user);
        Assert.assertEquals(usercontroller.getUser(1),user);
        verify(restTemplate,atLeastOnce()).getForObject(url ,User.class ,1);

//        anyInt gives error here..

    }

   @Test
   public void testDeleteUser()
   {
      User user = new User();
      user = users.get(0);
//      doNothing().when(userLogic).deleteUser(anyInt());
       final String url = "http://localhost:8080/USER-1.0-SNAPSHOT/userservice/users/{userid}.do";
       doNothing().when(restTemplate).delete(url,1);

      ModelAndView model = new ModelAndView("redirect:/user.do");
      ModelAndView delete_model = usercontroller.deleteuser(1);

      Assert.assertEquals(model.getViewName() , delete_model.getViewName());

      verify(restTemplate,atLeastOnce()).delete(url,1);
    }

   @Test
   public void testAddUser()
   {
       MockHttpServletRequest request = new MockHttpServletRequest() ;
       request.setParameter("mobile" , "123123");
       request.setParameter("other" , "321321");
       request.setParameter("addadrline1" , "ahmedabad");

       User user = new User();

       user = users.get(0);  // here is it necessary that we dont have id for user ??

       final String url = "http://localhost:8080/USER-1.0-SNAPSHOT/userservice/users.do";
       doNothing().when(restTemplate).postForLocation(url ,user);
//       doNothing().when(userLogic).addUser(user);
       ModelAndView model = usercontroller.addUser(user,request);
       Assert.assertEquals(model.getViewName() , "redirect:/user.do");
       verify(restTemplate , atLeastOnce()).postForLocation(url,user);

   }

    @Test
   public void testUpdateUser()
   {
       MockHttpServletRequest request = new MockHttpServletRequest() ;
       request.setParameter("editphonemob" , "123123");
       request.setParameter("editphoneother" , "321321");
       request.setParameter("editadrline1" , "ahmedabad");
       request.setParameter("editmobid" , "1");
       request.setParameter("editphoneothid" , "2");
       request.setParameter("editadrid" , "1");

       final String url = "http://localhost:8080/USER-1.0-SNAPSHOT/userservice/users/{userid}.do";

       User user = new User();
       user = users.get(0);

       //doNothing().when(userLogic).updateUser(user);

       doNothing().when(restTemplate).put(url,user,1);

       Assert.assertEquals(usercontroller.updateUser(user,request).getViewName(),"redirect:/user.do");
       verify(restTemplate , atLeastOnce()).put(url,user,1);

   }





    public List<User> getUserList()
    {
        User user = new User();
        user.setId(1);
        user.setFname("Unnati");
        user.setLname("Bhavsar");
        user.setEmail("Ur.bh");


        Address address = new Address();
        address.setAdrid(1);
        address.setAdrline1("Saraspur");

        List<Phone> phones = new ArrayList<Phone>();

        Phone phone = new Phone();
        phone.setPhoneid(1);
        phone.setPhoneNo(123);

        phones.add(phone);

        phone = new Phone();
        phone.setPhoneid(2);
        phone.setPhoneNo(223);

        phones.add(phone);

        user.setPhones(phones);
        user.setAddress(address);

        users.add(user);

         user = new User();
        user.setId(2);
        user.setFname("Ronak");
        user.setLname("Bhavsar");
        user.setEmail("rb.bh");


        address = new Address();
        address.setAdrid(2);
        address.setAdrline1("Saraspur");

         phones = new ArrayList<Phone>();

         phone = new Phone();
        phone.setPhoneid(2);
        phone.setPhoneNo(223);

        phones.add(phone);

        phone = new Phone();
        phone.setPhoneid(3);
        phone.setPhoneNo(323);

        phones.add(phone);

        user.setPhones(phones);
        user.setAddress(address);

        users.add(user);


        return users;
    }


}
