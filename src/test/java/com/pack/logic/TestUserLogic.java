package com.pack.logic;

import com.pack.hibernatedao.UserDao;
import com.pack.model.Address;
import com.pack.model.Phone;
import com.pack.model.User;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

import org.testng.annotations.Test;

/**
 * Created by rbhavsar on 1/22/16.
 */
public class TestUserLogic {

    @Mock
    UserDao userDao;

    @InjectMocks
    UserLogicImpl userLogic;

    @Spy
    List<User> users = new ArrayList<User>();

    @BeforeClass
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        users = getUserList();
    }

   @Test
   public void testGetList()
   {

    when(userDao.getAllUsers()).thenReturn(users);
    Assert.assertEquals(userLogic.getlist(),users);
    verify(userDao,atLeastOnce()).getAllUsers();

   }

   @Test
   public void testGetUser()
   {
       User user = new User();
       user = users.get(1);
       when(userDao.getuser(1)).thenReturn(user);
       Assert.assertEquals(userLogic.getUser(1),user);
       verify(userDao,atLeastOnce()).getuser(anyInt());
   }

   @Test
   public void testDeleteUser()
   {
        doNothing().when(userDao).deleteUser(anyInt());
        userLogic.deleteUser(anyInt());
        verify(userDao,atLeastOnce()).deleteUser(anyInt());
   }

   @Test
   public void testAddUser()
   {
       User user = new User();
       user = users.get(1);
       doNothing().when(userDao).addUser(user);
       userLogic.addUser(user);
       verify(userDao,atLeastOnce()).addUser(user);
   }

   @Test
   public void testUpdateUser()
   {
       User user = new User();
       user = users.get(1);
       doNothing().when(userDao).updateUser(user);
       userLogic.updateUser(user);
       verify(userDao,atLeastOnce()).updateUser(user);

   }

    private List<User> getUserList() {

        User user = new User();
        user.setId(1);
        user.setFname("Unnati");
        user.setLname("Bhavsar");
        user.setEmail("Ur.bh");


        Address address = new Address();
        address.setAdrid(1);
        address.setAdrline1("Saraspur");

        List<Phone> phones = new ArrayList<Phone>();

        Phone phone = new Phone();
        phone.setPhoneid(1);
        phone.setPhoneNo(123);

        phones.add(phone);

        phone = new Phone();
        phone.setPhoneid(2);
        phone.setPhoneNo(223);

        phones.add(phone);

        user.setPhones(phones);
        user.setAddress(address);

        users.add(user);

        user = new User();
        user.setId(2);
        user.setFname("Ronak");
        user.setLname("Bhavsar");
        user.setEmail("rb.bh");


        address = new Address();
        address.setAdrid(2);
        address.setAdrline1("Saraspur");

        phones = new ArrayList<Phone>();

        phone = new Phone();
        phone.setPhoneid(2);
        phone.setPhoneNo(223);

        phones.add(phone);

        phone = new Phone();
        phone.setPhoneid(3);
        phone.setPhoneNo(323);

        phones.add(phone);

        user.setPhones(phones);
        user.setAddress(address);

        users.add(user);


        return users;




    }


}
