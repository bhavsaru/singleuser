//package com.pack.logic;
//
//import com.pack.model.Address;
//import com.pack.model.Phone;
//import com.pack.model.User;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//
//import javax.jws.soap.SOAPBinding;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by rbhavsar on 1/20/16.
// */
//@ContextConfiguration("classpath:user-servlet.xml")
//@WebAppConfiguration
//@RunWith(SpringJUnit4ClassRunner.class)
//public class UserLogicImplTest {
//    @Autowired
//    UserLogic userLogic;
//
//    @org.junit.Test
//    public void testLogic() throws Exception {
//
//        User user = new User();
//        user.setFname("hello3");
//        user.setLname("lname");
//        user.setPhones(new ArrayList<Phone>());
//        user.getPhones().add(new Phone(1231231, "mob"));
//        user.getPhones().add(new Phone(1231231, "oth"));
//
//        Address address = new Address();
//        address.setAdrline1("Adrline1");
//
//        user.setAddress(address);
////        user.getAddress().setAdrline1("Adrline1");
//
//        userLogic.addUser(user);
//
//    }
//
//    @Test
//    public void testLazy() throws Exception {
//        User user = userLogic.getUser(34);
//        System.out.println(user.getPhones().get(0).getPhoneid());
//
//    }
//
//    @Test
//    public void testUpdate() throws Exception{
//        User user = new User();
//        Address adr = new Address();
//        List<Phone> phones = new ArrayList<Phone>();
//
//        Phone phone = new Phone();
//        phone.setPhoneNo(11111);
//
//        phones.add(phone);
//        phone = new Phone();
//        phone.setPhoneNo(22222);
//
//        phones.add(phone);
//
//        adr.setAdrline1("TestAdr");
//
//        user.setFname("Un");
//        user.setAddress(adr);
//        user.setPhones(phones);
//
//        userLogic.updateUser(user);
//
//
//    }
//
//
//
//
//
//}
