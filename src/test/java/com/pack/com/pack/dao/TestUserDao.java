package com.pack.com.pack.dao;

import com.pack.hibernatedao.UserDao;
import com.pack.hibernatedao.UserDaoImpl;
import com.pack.model.Address;
import com.pack.model.Phone;
import com.pack.model.User;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import org.testng.annotations.Test;
import static org.mockito.Mockito.*;

/**
 * Created by rbhavsar on 1/22/16.
 */
public class TestUserDao {

  @Mock
  EntityManager entityManager;

  @InjectMocks
  UserDaoImpl userDao;

  @Spy
  List<User> users = new ArrayList<User>();

  @BeforeClass
  public void setUp()
  {
      MockitoAnnotations.initMocks(this);
      users = getUserList();
  }

   @Test
   public void testGetAllUsers()
   {
        Query query = mock(Query.class);
        when(entityManager.createQuery("from User u")).thenReturn(query);
        when(query.getResultList()).thenReturn(users);

       Assert.assertEquals(userDao.getAllUsers() ,users);
       verify(entityManager,atLeastOnce()).createQuery("from User u");

   }

   @Test
   public void testGetUser()
   {
       User user = users.get(0);
       when(entityManager.find(User.class, 1)).thenReturn(user);   // not working with anyInt method

       Assert.assertEquals(userDao.getuser(1) ,user);
       verify(entityManager , atLeastOnce()).find(User.class ,1);
   }

  @Test
   public void testDeleteEmployee()
   {
      User user = users.get(0);
      when(entityManager.find(User.class , 1)).thenReturn(user);
      doNothing().when(entityManager).remove(user);
      userDao.deleteUser(1);
       verify(entityManager ,atLeastOnce()).remove(user);
   }

    @Test
    void testAddUser()
    {
        User user = users.get(0);
        doNothing().when(entityManager).persist(user);
        userDao.addUser(user);
        verify(entityManager ,atLeastOnce()).persist(user);
    }

    @Test
    void testUpdateUser()
    {
        User user = users.get(0);
        when(entityManager.merge(user)).thenReturn(user);
        userDao.updateUser(user);
        verify(entityManager , atLeastOnce()).merge(user);

    }


    private List<User> getUserList() {

        User user = new User();
        user.setId(1);
        user.setFname("Unnati");
        user.setLname("Bhavsar");
        user.setEmail("Ur.bh");


        Address address = new Address();
        address.setAdrid(1);
        address.setAdrline1("Saraspur");

        List<Phone> phones = new ArrayList<Phone>();

        Phone phone = new Phone();
        phone.setPhoneid(1);
        phone.setPhoneNo(123);

        phones.add(phone);

        phone = new Phone();
        phone.setPhoneid(2);
        phone.setPhoneNo(223);

        phones.add(phone);

        user.setPhones(phones);
        user.setAddress(address);

        users.add(user);

        user = new User();
        user.setId(2);
        user.setFname("Ronak");
        user.setLname("Bhavsar");
        user.setEmail("rb.bh");


        address = new Address();
        address.setAdrid(2);
        address.setAdrline1("Saraspur");

        phones = new ArrayList<Phone>();

        phone = new Phone();
        phone.setPhoneid(2);
        phone.setPhoneNo(223);

        phones.add(phone);

        phone = new Phone();
        phone.setPhoneid(3);
        phone.setPhoneNo(323);

        phones.add(phone);

        user.setPhones(phones);
        user.setAddress(address);

        users.add(user);

        return users;

    }


}
