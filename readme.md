This project is a single page user application.The purpose of this project is to give overview over below technologies.

Spring MVC,
JPA-Hibernate,
Spring Rest Services,
MAVEN,
Spring Exception Handling

This project has Design patterns like DAO and MVC implemented.

Two DAO layers have been implemented one is with JDBC template and the other is with JPA-Hibernate.

There are model classes named User, Address and Phone. And Mapping between those classes is also implemented
in hibernate.

One view file is there in webapps/WEB-INF/jsp folder with name userlist.jsp. This file shows list of users and provides
interface to update, delete and insert user information.

Controller is created inside controller folder which provides mapping between URI of the http request and methods of the controller.

From controller the call to REST api methods has been implemented using client side support for REST services(Rest Template).

Global Exception handler and Controller level exception handlers of spring are also used.

